public class ExtendedEuclid {

    private static long[] extendedEuclid(long a, long b) {
        long q, r, x1 = 0, x2 = 1, y1 = 1, y2 = 0, x, y;

        if (b == 0) {
            return new long[]{a, 1, 0};
        }

        while (b > 0) {
            q = a / b;
            r = a - q * b;

            x = x2 - q * x1;
            y = y2 - q * y1;

            a = b;
            b = r;

            x2 = x1;
            x1 = x;
            y2 = y1;
            y1 = y;
        }

        return new long[]{a, x2, y2};
    }

    public static long solveLinearEquation(long a, long b, long n) {
        long[] dxy = extendedEuclid(a, n);
        long d = dxy[0], x = dxy[1], y = dxy[2];
        if (b % d == 0) {
            long x0 = (x * (b / d)) % n;
            return x0;
            /*for (int i = 0; i < d; i++) {
                long res = (x0 + i * (n / d)) % n;
                System.out.println(res);
                return res;
            }*/
        }
        return 0;
    }
}